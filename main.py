import sys
from AnalisadorLexico.scanner import Scanner
from AnalisadorLexico.tabela_de_simbolos import Tabela_de_simbolos
from AnalisadorSintatico.parser import *

# Leitura do arquivo do terminal
leitura_terminal = sys.argv

# Inicializa a tabela de simbolos
tabela_de_simbolos = Tabela_de_simbolos()

# Analise Sintatica

# analisador_sintatico = Parser(leitura_terminal[1], tabela_de_simbolos)
# analisador_sintatico.parse()


#FORMA 1 DE RODAR
# Chama a funcao scanner passando nossa tabela de simbolos, e o arquivo que irá ler
# scanner = Scanner(leitura_terminal[1], tabela_de_simbolos)
# scanner.executa()
# for i in scanner.lista_de_tokens:
#     print(i)

# FORMA 2 DE RODAR
# scanner = Scanner(leitura_terminal[1], tabela_de_simbolos)
# while(not scanner.arquivo_acabou):
#     print(scanner.getNext())



scanner = Scanner('teste.txt', tabela_de_simbolos)
analisador_sintatico = Parser(scanner)
analisador_sintatico.parse()