from AnalisadorLexico.token import Token


class Tabela_de_simbolos():
    
    tabela: list[Token]
    
        
    def __init__(self):
        self.tabela = []
        self._inserir_palavras_reservadas()
        
    def _inserir_palavras_reservadas(self):
        
        # Tokens = (Lexema,Classe, Tipo de Dado)
        self.tabela.append(Token("inicio","inicio","inicio"))
        self.tabela.append(Token("varinicio","varinicio","varinicio"))
        self.tabela.append(Token("varfim", "varfim", "varfim"))
        self.tabela.append(Token("escreva", "escreva", "escreva"))
        self.tabela.append(Token("leia", "leia", "leia"))
        self.tabela.append(Token("se", "se", "se"))
        self.tabela.append(Token("entao", "entao", "entao"))
        self.tabela.append(Token ("fimse", "fimse", "fimse"))
        self.tabela.append(Token("repita", "repita", "repita"))
        self.tabela.append(Token("fimrepita", "fimrepita", "fimrepita"))
        self.tabela.append(Token("fim", "fim", "fim"))
        self.tabela.append(Token("inteiro", "inteiro", "inteiro"))
        self.tabela.append(Token("literal", "literal", "literal"))
        self.tabela.append(Token("real", "real", "real"))
        
    def inserir_token(self, token:Token):
        token_existe = self.buscar_token(token)
        if(token_existe is None):
            if(token.classe == "id"):
                self.tabela.append(token)
            return token
        else:
            return token_existe
        
    def buscar_token(self, token:Token):
        for x in self.tabela:
            if(x.lexema == token.lexema):
                return x
        return None
    
    def atulizar_tabela(self, token: Token):
        if(token.classe == "id"):
            elemento_da_lista = self.buscar_token(token)
            if(elemento_da_lista):
                self.tabela.remove(elemento_da_lista)
                self.tabela.append(token)
        
    def imprimir_tabela(self):
        for x in self.tabela:
            print(x)