from ast import Str
from AnalisadorLexico.tabela_de_estados import mudanca_estado, temCaracter
from AnalisadorLexico.tabela_de_simbolos import Tabela_de_simbolos

from AnalisadorLexico.token import Token

class criadorDeTokens():
    
    lexema: Str
    estado: int
    
    def __init__(self):
        self.lexema = None
        self.estado = 0
        
    def temCaracter(self):
        return temCaracter(self.lexema)
        
    def adicionarCaracter(self, novo_caracter, tabela_de_simbolos : Tabela_de_simbolos):
        
        if(self.estado == 0):
            self.__reset()
        
        estado_atual = mudanca_estado(self.estado, novo_caracter)
        
        if(estado_atual != 25):
            self.estado = estado_atual
            if(self.lexema is not None):
                self.lexema = self.lexema + novo_caracter
            else:
                self.lexema = novo_caracter
        else:
            estado_temporario = mudanca_estado(0, novo_caracter)
            if(estado_temporario == 25):
                return 25
            
            elif(estado_temporario == 0):
                return self.gerar_token_e_inserir_na_tabela(tabela_de_simbolos)
            
            else:
                token = self.gerar_token_e_inserir_na_tabela(tabela_de_simbolos)
                self.lexema = novo_caracter
                self.estado = estado_temporario
                return token
        
        return self.estado
       
    def finalizar_arquivo(self, tabela_de_simbolos : Tabela_de_simbolos):
        self.lexema = "EOF"
        self.estado = 15
        return self.gerar_token_e_inserir_na_tabela(tabela_de_simbolos)
        
    def gerar_token_e_inserir_na_tabela(self, tabela_de_simbolos : Tabela_de_simbolos):
        token = Token(self.lexema, self.estado)
        token_gerado = tabela_de_simbolos.inserir_token(token)
        self.__reset()
        return token_gerado
    
    def __reset(self):
        self.lexema = None
        self.estado = 0
