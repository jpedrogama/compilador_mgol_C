# Funcoes auxiliares
def e_letra(caracter):
    return (caracter>='a' and caracter<='z') or (caracter>='A' and caracter<='Z')

def temCaracter(caracter: str):
    if(caracter is not None):
        if (not caracter.isspace()):
            return True
    return False

#Retorna estado e se é um terminal
def mudanca_estado(estado_atual, caracter: str):
    
    if estado_atual == 0:
        if(caracter.isdigit()):
            return 1
        if(caracter == '"'):
            return 7
        if(e_letra(caracter)):
            return 10
        if(caracter == "{"):
            return 12
        if(caracter == ">"):
            return 16
        if(caracter == "<"):
            return 17
        if(caracter == "+" 
           or caracter == "-"
           or caracter == "*"
           or caracter == "/"):
            return 18
        if(caracter == "("):
            return 19
        if(caracter == ")"):
            return 20
        if(caracter == ";"):
            return 21
        if(caracter == ","):
            return 22
        if(caracter == "="):
            return 24
        if(not temCaracter(caracter)):
            return 0
        
        return 25
        
        
    elif estado_atual == 1:
        if(caracter.isdigit()):
            return 1
        if(caracter == "."):
            return 2
        if(caracter == "E" or caracter =="e"):
            return 4
        
        return 25
        
    elif estado_atual == 2:
        if(caracter.isdigit()):
            return 3
        return 25
    
    elif estado_atual == 3:
        if(caracter.isdigit()):
            return 3
        if(caracter == "E" or caracter == "e"):
            return 4
        return 25
    
    elif estado_atual == 4:
        if(caracter == "-"):
            return 6
        if(caracter == "+"):
            return 26
        if(caracter.isdigit()):
            return 5
        return 25
    
    elif estado_atual == 5:
        if(caracter.isdigit()):
            return 5
        return 25
   
    elif estado_atual == 6:
        if(caracter.isdigit()):
            return 5
        return 25
    
    elif estado_atual == 7:
        if(caracter == '"'):
            return 9    
        if(caracter):
            return 8
        return 25
    
    elif estado_atual == 8:
        if(caracter == '"'):
            return 9    
        if(caracter):
            return 8
        return 25
    
    elif estado_atual == 9:
        if(caracter):
            return 25
    
    elif estado_atual == 10:
        if(e_letra(caracter) or caracter.isdigit() or caracter=="_"):
            return 11 
        return 25
    
    elif estado_atual == 11:
        if(e_letra(caracter) or caracter.isdigit() or caracter=="_"):
            return 11 
        return 25
    
    elif estado_atual == 12:
        if(caracter == '}'):
            return 13    
        if(caracter or caracter.isspace()):
            return 14
        return 25
    
    elif estado_atual == 13:
        if(caracter):
            return 25
    
    elif estado_atual == 14:
        if(caracter == '}'):
            return 13    
        if(caracter or caracter.isspace()):
            return 14
        return 25
        
    elif estado_atual == 15:
        if(caracter):
            return 25
    
    elif estado_atual == 16:
        if(caracter == "="):
            return 24
        return 25
    
    elif estado_atual == 17:
        if(caracter == ">"
           or caracter == "="):
            return 24
        if(caracter == "-"):
            return 23
        return 25
    
    elif estado_atual == 18:
        if(caracter):
            return 25 
    
    elif estado_atual == 19:
        if(caracter):
            return 25
    
    elif estado_atual == 20:
        if(caracter):
            return 25
    
    elif estado_atual == 21:
        if(caracter):
            return 25
    
    elif estado_atual == 22:
        if(caracter):
            return 25
    
    elif estado_atual == 23:
        if(caracter):
            return 25
    
    elif estado_atual == 24:
        if(caracter):
            return 25
        
    elif estado_atual == 26:
        if(caracter):
            return 1
    