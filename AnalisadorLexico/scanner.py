from itertools import islice
from AnalisadorLexico.criadorDeTokens import criadorDeTokens
from AnalisadorLexico.token import Token

class Scanner():

    lista_de_tokens: list[Token]
    linha_contador: int
    caracter_contador: int
    lexema: criadorDeTokens
    continue_lendo: bool
    arquivo_acabou: bool

    def __init__(self, arquivo_fonte, tabela_de_simbolos):
        self.arquivo_fonte = arquivo_fonte
        self.tabela_de_simbolos = tabela_de_simbolos
        self.lista_de_tokens = []
        self.lexema = criadorDeTokens()
        self.linha_contador = 0
        self.caracter_contador = 0
        self.continue_lendo = True
        self.arquivo_acabou= False

    def executa(self):
        arquivo_fonte = open(self.arquivo_fonte, encoding="utf-8")
        lexema: criadorDeTokens = criadorDeTokens()
        linha_contador = 0
        caracter_contador= 0
        for linha in arquivo_fonte:
            linha_contador = linha_contador+1
            caracter_contador = 0
            for caracter in linha:
                caracter_contador = caracter_contador +1
                elemento = lexema.adicionarCaracter(caracter, self.tabela_de_simbolos)
                
                if(elemento == 25):
                    print("\nERRO LÉXICO - Caractere inválido na linguagem, linha {}, coluna {} \n".format(linha_contador, caracter_contador))
                    #raise SystemExit()
                
                if (isinstance(elemento, Token)):
                    if(elemento.classe == "ERRO"):
                        print("\nERRO LÉXICO - Caractere inválido na linguagem, linha {}, coluna {} \n".format(linha_contador, caracter_contador))
                        #raise SystemExit()
                    else:    
                        self.lista_de_tokens.append(elemento)
        
        if(lexema.estado !=0):
            self.lista_de_tokens.append(lexema.gerar_token_e_inserir_na_tabela(self.tabela_de_simbolos))   
        
        self.lista_de_tokens.append(criadorDeTokens().finalizar_arquivo(self.tabela_de_simbolos))
        arquivo_fonte.close()
        
    
    def getNext(self):
        self.continue_lendo = True
        arquivo_fonte = open(self.arquivo_fonte, encoding="utf-8")
        
        while(self.continue_lendo):
            for linha in islice(arquivo_fonte, self.linha_contador, None):
                for caracter in islice(linha, self.caracter_contador, None):
                    self.caracter_contador = self.caracter_contador+1
                    if(self.caracter_contador >= len(linha)):
                        self.linha_contador = self.linha_contador+1
                        self.caracter_contador = 0
                    
                    elemento = self.lexema.adicionarCaracter(caracter, self.tabela_de_simbolos)
                    if(elemento == 25):
                        print("\nERRO LÉXICO - Caractere inválido na linguagem, linha {}, coluna {} \n".format(self.linha_contador, self.caracter_contador))
                        #raise SystemExit()
                    
                    if(isinstance(elemento, Token)):
                        if(elemento.classe == "ERRO"):
                            print("\nERRO LÉXICO - Caractere inválido na linguagem, linha {}, coluna {} \n".format(self.linha_contador, self.caracter_contador))
                            #raise SystemExit()
                        else:
                            self.lista_de_tokens.append(elemento)
                            self.continue_lendo = False
                            if(elemento.classe == 'Comentario'):
                                return None
                            else:
                                elemento.linha = self.linha_contador
                                elemento.coluna = self.caracter_contador
                            return elemento
                    
                    
                

            if(self.lexema.estado !=0):
                token_gerado = self.lexema.gerar_token_e_inserir_na_tabela(self.tabela_de_simbolos)
                self.lista_de_tokens.append(token_gerado)
                if(token_gerado.classe == 'Comentario'):
                    return None
                # else:
                #     elemento.linha = self.linha_contador
                #     elemento.coluna = self.caracter_contador
                return token_gerado
            
            token_finalizador = criadorDeTokens().finalizar_arquivo(self.tabela_de_simbolos)
            self.lista_de_tokens.append(token_finalizador)
            self.arquivo_acabou = True
            if(token_finalizador.classe == 'Comentario'):
                return None
            return token_finalizador
        
        arquivo_fonte.close()
