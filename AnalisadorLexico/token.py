from ast import Str

class Token():
    lexema: Str
    classe: Str
    tipo_de_dado: Str
    linha: int
    coluna: int
    
    def __init__(self, lexema: Str, estado: int or Str, linha=0, coluna=0, tipo_de_dado: Str = None):
        self.lexema = lexema
        self.linha = linha
        self.coluna = coluna
        if(isinstance(estado, str)):
            self.classe = estado
            self.tipo_de_dado = tipo_de_dado
        else:
            classe, tipo = self.__definir_classe_tipo(estado)
            self.classe = classe
            self.tipo_de_dado = tipo   
        
    def __str__(self):
        return "Lexema: {}, Classe: {}, Tipo: {}.".format(self.lexema, self.classe, self.tipo_de_dado)
    
    def __definir_classe_tipo(self, estado: int):
        
        if(estado==0):
            return None, None
        
        if(estado == 1):
            return "Num", "inteiro"
           
        if (estado == 3
           or estado == 5):
            return "Num", "real"
        
        if(estado == 9):
            return "Lit", "literal"
        
        if(estado == 10
           or estado == 11):
            return "id", None
        
        if(estado == 13):
            return "Comentario", None
        
        if(estado == 15):
            return "EOF", None
        
        if(estado == 16 or estado==17 or estado == 24):
            return "OPR", None
        
        if(estado == 18):
            return "OPM", None
        
        if(estado == 19):
            return "AB_P", None
        
        if(estado == 20):
            return "FC_P", None
        
        if(estado == 21):
            return "PT_V", None
        
        if(estado == 22):
            return "VIR", None
        
        if(estado == 23):
            return "RCB", None
        
        return "ERRO", "ERRO"
        
        