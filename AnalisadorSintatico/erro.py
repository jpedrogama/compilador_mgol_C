from dataclasses import asdict
from AnalisadorLexico.token import Token
from AnalisadorSintatico.pegar_action import PegarAction
from AnalisadorSintatico.pilha import Item_Pilha, Pilha


values_move_forward = { "FC_P": ")", "PT_V": ";", "AB_P": "(", "fimse": "fimse", "RCB": "<-", "fimrepita": "fimrepita", "varfim": "varfim", "varinicio": "varinicio", "leia": "leia", "escreva": "escreva", "se": "se"}


class RotinaErro():
    
    def panic(self, pilha:Pilha, next_element, a, erro_list):
        try:
            pilha_retorno = pilha
            actions_controller = PegarAction()
            
            action = actions_controller.get_action(pilha_retorno.recebe_topo(),next_element.classe)
            
            if (not action.acao == ''):
                erro_list.append("ERRO -> Linha:{} Coluna:{} - MODO PANICO".format( a.linha, a.coluna, a.classe))
                return next_element
            
        except:
            return None
    
    def forward_move_pennello(self, pilha_last:int, erro_list, a):
        try:
            
            actions_controller = PegarAction()
            
            for key in values_move_forward:
                action = actions_controller.get_action(pilha_last, key)
                if (not action== '' and not action.acao == 'erro'):
                    
                    msg_erro = "ERRO -> Linha:{} Coluna:{} - Falta um {}".format(a.linha, a.coluna, key)
                    erro_list.append(msg_erro)
                    
                    return Token(key, key, key)
                        
        except:
            return None
            

    def forward_move_pennello_remove(self, pilha:Pilha, next_element, a, erro_list):
        try:
            pilha_retorno = pilha
            actions_controller = PegarAction()
            
            action = actions_controller.get_action(pilha_retorno.recebe_topo(),next_element.classe)
            
            if (not action.acao == ''):
                erro_list.append("ERRO -> Linha:{} Coluna:{} - Caracter não esperado {} ".format( a.linha, a.coluna, a.classe))
                return next_element
            
        except:
            return None