import re
from AnalisadorSintatico.action import ActionObject
import csv

class PegarAction():
    
    def __init__(self):
        tabela_para_salvar = []
        with open('AnalisadorSintatico/tabelas/tabela_acoes.csv') as acoes_tabela:
            infos = csv.DictReader(acoes_tabela, delimiter=';') 
            for linha in infos:
                tabela_para_salvar.append(linha)
        self.tabela = tabela_para_salvar
    
    def get_action(self, s, a) -> ActionObject:
        value = self.tabela[s.pos][a]
        
        if (value is not None and not value.isspace()):
            if value == 'acc':
                return ActionObject(acao='acc', estado =int(0))
            if value == '':
                return ActionObject(acao='erro', estado =int(99999))
            else:
                acao, index, n = re.split('(\d+)',value)
                return ActionObject(acao=acao, estado =int(index))
    

index_acao = {
    'estado': 0,
    'inicio': 1,
    'varinicio': 2,
    'varfim': 3,
    'PT_V': 4,
    'id': 5,
    'VIR': 6,
    'inteiro': 7,
    'real': 8,
    'literal': 9,
    'leia': 10,
    'escreva': 11,
    'Lit': 12,
    'Num': 13,
    'RCB': 14,
    'OPM': 15,
    'se': 16,
    'AB_P': 17,
    'FC_P': 18,
    'entao': 19,
    'POR': 20,
    'fimse': 21,
    'repita': 22,
    'fimrepita': 23,
    'fim': 24,
    '$': 25
}