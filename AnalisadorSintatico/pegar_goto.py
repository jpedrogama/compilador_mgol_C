import csv


class PegarGoto():
    
    def __init__(self):
        tabela_para_salvar = []
        with open('AnalisadorSintatico/tabelas/tabela_goto.csv') as goto_tabela:
            infos = csv.DictReader(goto_tabela, delimiter=';') 
            for linha in infos:
                tabela_para_salvar.append(linha)
        self.tabela = tabela_para_salvar
        
    def get_goto(self, t, A):
        value = self.tabela[t.pos][A]
        
        if(value is not None and not value.isspace()):
            return value
        


index_goto ={
    "estado": 0,
    "P'": 1,
    "P": 2,
    "V": 3,
    "LV": 4,
    "D": 5,
    "L": 6,
    "TIPO": 7,
    "A": 8,
    "ES": 9,
    "ARG": 10,
    "CMD": 11,
    "LD": 12,
    "OPRD": 13,
    "COND": 14,
    "CAB": 15,
    "EXP_R": 16,
    "CP": 17,
    "R": 18,
    "CABR": 19,
    "CPR": 20
}