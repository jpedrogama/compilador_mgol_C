from AnalisadorLexico.scanner import Scanner
from AnalisadorSintatico.erro import RotinaErro
from AnalisadorSintatico.operacoes import operacoes
from AnalisadorSintatico.pegar_action import PegarAction
from AnalisadorSintatico.pegar_goto import PegarGoto
from AnalisadorSintatico.pilha import Item_Pilha, Pilha


class Parser():
    
    scanner: Scanner
    pilha: Pilha
    
    def __init__(self, scanner):
        self.scanner = scanner
        self.pilha = Pilha()
        self.pegar_action = PegarAction()
        self.pegar_goTo = PegarGoto()
        self.erros = []
        self.recupera_erro = None
        self.isPanic = False

    def parse(self):
        
        # Receber token
        a = self.scanner.getNext()
                
        while True:

            
            s = self.pilha.recebe_topo()
            action = self.pegar_action.get_action(s,a.classe)
            
            # Shift
            if (action.acao == 's'):
                t = action.estado
                self.pilha.empilha(Item_Pilha(pos =t, terminal = a.classe))
                
                if(not self.recupera_erro):
                    a = self.scanner.getNext()
                else:
                    a = self.recupera_erro
                    self.recupera_erro =None
                    
                if self.isPanic:
                    self.isPanic = False
                
            # Reduce
            elif (action.acao == 'r'):
                A, B, producao = operacoes(action.estado)
                self.desempilha_todos(B)
                t = self.pilha.recebe_topo()
                
                valor_goTo = int(self.pegar_goTo.get_goto(t, A))
                self.pilha.empilha(Item_Pilha(pos = valor_goTo, terminal= A))
                print(producao)
                if self.isPanic:
                    self.isPanic = False
                
            
            # Aceita
            elif (action.acao == 'acc'):
                print('Analise Sintatica Finalizada com Sucesso!')
                
                if self.erros:
                    for erro in self.erros:
                        print(erro)
                break
            
            ## Recupera erro
            else:
                
                rotinas_erro = RotinaErro()
                
                # next_token = self.scanner.getNext()
                # whats_next = rotinas_erro.forward_move_pennello_remove(self.pilha, next_token, a=a, erro_list = self.erros)
                
                # if whats_next:
                #     a = whats_next
                
                # else:    
                #     token_recuperado = rotinas_erro.forward_move_pennello(pilha_last = s, erro_list = self.erros, a =a)
                #     if( token_recuperado): #action and not action.acao == 'erro'):
                #         self.recupera_erro = a
                #         a = token_recuperado
                
                if self.isPanic:
                    next_token = self.scanner.getNext()
                    panic_response = rotinas_erro.panic(self.pilha, next_token, a=a, erro_list = self.erros)
                    if panic_response:
                        a = panic_response
                
                else:
                    token_recuperado = rotinas_erro.forward_move_pennello(pilha_last = s, erro_list = self.erros, a =a)
                    
                    if( token_recuperado): #action and not action.acao == 'erro'):
                        self.recupera_erro = a
                        a = token_recuperado
                    
                    else:
                        next_token = self.scanner.getNext()
                        whats_next = rotinas_erro.forward_move_pennello_remove(self.pilha, next_token, a=a, erro_list = self.erros)
                        
                        if whats_next:
                            a = whats_next
                        else:
                            self.isPanic = True
                            panic_response = rotinas_erro.panic(self.pilha, next_token, a=a, erro_list = self.erros)
                            if panic_response:
                                a = panic_response
                    
                    ### SEPARA
                    # next_token = self.scanner.getNext()
                    # whats_next = rotinas_erro.forward_move_pennello_remove(self.pilha, next_token, a=a, erro_list = self.erros)
                    
                    # if whats_next:
                    #     a = whats_next
                    
                    # else:    
                    #     token_recuperado = rotinas_erro.forward_move_pennello(pilha_last = s, erro_list = self.erros, a =a)
                    #     if( token_recuperado): #action and not action.acao == 'erro'):
                    #         self.recupera_erro = a
                    #         a = token_recuperado
                        
                    #     else:
                    #         self.isPanic = True
                    #         panic_response = rotinas_erro.panic(self.pilha, next_token, a=a, erro_list = self.erros)
                    #         if panic_response:
                    #             a = panic_response
                    

    
    def desempilha_todos(self, B):
        for value in reversed(B):
            self.pilha.desempilha_baseado_palavra(value)
  


    