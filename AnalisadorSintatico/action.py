from ast import Str


class ActionObject():
    
    acao: Str
    estado: int
    
    def __init__(self, acao: Str, estado: int) -> None:
        self.acao = acao
        self.estado = estado