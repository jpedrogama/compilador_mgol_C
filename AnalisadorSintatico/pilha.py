from typing import List

class Item_Pilha():
    def __init__(self, pos, terminal):
        self.pos = pos
        self.terminal = terminal

class Pilha():
    
    pilha_list: List[Item_Pilha]
    
    def __init__(self):
        self.pilha_list = [Item_Pilha(0, 0)]
        
    def recebe_topo(self):
        return self.pilha_list[-1]

    # Empilha token no topo da pilha
    def empilha(self, token):
        self.pilha_list.append(token)

    # Desempilha token do topo da pilha
    def desempilha(self):
        self.pilha_list.pop(-1)
    
    # Desempilha token do topo da pilha
    def desempilha_baseado_palavra(self, value):
        if(self.pilha_list[-1].terminal.upper() == value.upper()):
            self.desempilha()
        
        
